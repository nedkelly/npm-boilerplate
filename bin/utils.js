
// Utils

var chalk         = require('chalk');
var fs            = require('fs');
var fsp           = require('fs-path');
var path          = require('path'),
    dirname       = path.dirname
    normalize     = path.normalize,
    extname       = path.extname,
    join          = path.join,
    resolve       = path.resolve;

// Export utilities

module.exports = {

  /*!
   * Normalize, resolve join and return the path
   *
   * @param  {Array} input
   * @return {String}
   */
  pathify: function(input) {
    return normalize(resolve(join.apply(this, input)));
  },

  /*!
   * Filter "hidden" `files`, aka files
   * beginning with a `.`.
   *
   * @param {Array} files
   * @return {Array}
   * @api private
   */
  filterHiddenFiles: function(files) {
    return files.filter(function(file) {
      return '.' != file[0];
    });
  },

  /*!
   * Get Files in `dir` recursively
   *
   * @param  {String} dir
   * @param  {Boolean} recursive
   * @param  {Array} filelist
   * @return {Array}
   */
  getFilesRecursive: function(dir, recursive, ext, filelist) {

    var self = this;

    var files;
    try {
     files = self.filterHiddenFiles(fs.readdirSync(dir));
    } catch (err) {
      // Assume missing directory...
    }
    filelist = filelist || [];

    files.forEach(function(file) {
      if (recursive && fs.statSync(dir + '/' + file).isDirectory()) {
        filelist = self.getFilesRecursive(dir + '/' + file, recursive, ext, filelist);
      }
      else {
        var filePath = normalize(resolve(join(dir, '/', file)));
        var stat = fs.stat(filePath);
        // var stat = fs.statSync(filePath);

        if (extname(filePath).match(ext)) {
          filelist.push({path: filePath, name: file});
        }
      }
    });

    return filelist;
  },

  /*!
   * writeFile including the directory structure
   *
   * @param  {String}   path
   * @param  {String}   contents
   * @param  {Object}   options
   * @param  {Function} cb
   * @return {Void}
   */
  writeFile: function(path, contents, options, cb) {
    // mkdirp(dirname(path), function(err) {
    fsp.mkdir(dirname(path), function(err) {
      if (err) return cb(err);
      fsp.writeFile(path, contents, options, cb);
    });
  }

};
