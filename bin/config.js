
var pkg = require('../package.json');

/**
 * isDev process environemnt variable
 *
 * @type {Boolean}
 */
module.exports.isDev = process.env.DEV_MODE === 'dev';

/**
 * JavaScript configuration object
 *
 * @type {Object}
 */
module.exports.js = {
  scripts: [
    'main.js',
    'libraries.js',
    'polyfills.js'
  ],
  input: './src/',
  output: './public/',
  path: 'assets/js/',
  watch: './src/assets/js/**/*.js'
};

/**
 * CSS configuration object.
 *
 * @type {Object}
 */
module.exports.css = {
  input:  './src/assets/css/screen.scss',
  output: './public/assets/css/screen.css',
  watch: './src/assets/css/**/*.scss'
};

/**
 * Swig configuration object.
 *
 * @type {Object}
 */
module.exports.swig = {
  folders: [
    { input: 'modules', output: 'modules', recursive: false },
    { input: 'pages', output: '', recursive: false }
  ],
  input: './src/',
  output: './public/',
  watch: './src/**/*.swig'
};

/**
 * Copy
 *
 * @type {Object}
 */
module.exports.copy = {
  folders: [
    'images',
    'fonts'
  ],
  input: './src/assets/',
  output: './public/assets/',
  watch: [
    './src/assets/images/**/*',
    './src/assets/fonts/**/*'
  ]
}

/**
 * Clean-up configuration. What to remove when running `gulp clean`.
 *
 * @type {Object}
 */
module.exports.clean = {
  input: './public/'
};

/**
 * Package configuration object.
 *
 * @type {Object}
 */
module.exports.package = {
  input: './public/**',
  output: './package/'
};

/**
 * BrowserSync configuration object.
 *
 * @type {Object}
 */
module.exports.browsersync = {
  reload: './public/**',
  input: './public/',
  projectServer: {
    //defaultPage: '/docs/README.html',
    localStorageSeed: 'charlie is my dog',
    menus: [
      {
        title: 'Pages',
        path: '/'
      },
      {
        title: 'Modules',
        path: '/modules'
      },
      {
        title: 'Documentation',
        path: '/docs'
      }
    ],
    pkg: {
      name: pkg.name,
      favicon: '/assets/images/charlie.jpg',
      version: pkg.version,
    }
  }
};

/**
 * Docs configuration object.
 *
 * @type {Object}
 */
module.exports.docs = {
  input: [
    { dir: '', recursive: false },
    { dir: '/src', recursive: true }
  ],
  output: './public/docs/',
  template: './bin/templates/docs.html',
  favicon: '../images/charlie.jpg',
  watch: [
    './*.md',
    './src/**/*.md'
  ],
};

/**
 * Watch configuration object.
 *
 * @type {Object}
 */
module.exports.watch = {
  tasks: [
    { path: module.exports.js.watch,   task: "npm run build:js" },
    { path: module.exports.css.watch,  task: "npm run build:css" },
    { path: module.exports.swig.watch, task: "npm run build:swig" },
    { path: module.exports.docs.watch, task: "npm run build:docs" },
    { path: module.exports.copy.watch, task: "npm run copy" }
  ]
};

/**
 * Banner string to be placed above JS and CSS files.
 *
 * @type {String}
 */
module.exports.banner = [
  '/**',
  ' * ' + pkg.name,
  ' * @version v' + pkg.version,
  ' * @timestamp ' + new Date(),
  ' */',
  ''
].join('\n');
