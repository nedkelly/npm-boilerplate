
"use strict";

// build:swig

var config        = require('../config');
var utils         = require('../utils.js');
var chalk         = require('chalk');
var swig          = require('swig');
var fs            = require('fs');

console.log(chalk.yellow('[npm run build:swig]'));

var ext = /(.swig)/g;

// Set `swig` options
swig.setDefaults({
  cache: false,
  varControls: ['{=', '=}'],
  loader: swig.loaders.fs()
});

// Loop through each `input` entry
config.swig.folders.forEach( function(s) {

  // Set the `filePath` relative to this `script`
  var filePath = utils.pathify([__dirname, '../../', config.swig.input, s.input]);

  // Get the files
  utils.getFilesRecursive(filePath, s.recursive, ext).map( function(file) {

    // Render the `swig`
    swig.renderFile(file.path, {}, function (err, output) {
      if (err) throw err;

      // Set the `outputPath`
      var outputPath = utils.pathify([config.swig.output, s.output, file.name.replace(ext, '.html')]);

      // Write this result to the file system
      utils.writeFile(outputPath, output, function(err) {
        if (err) throw err;
      });
    });
  });
});
