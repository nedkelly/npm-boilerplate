
"use strict";

// css:sass
var config        = require('../config');
var utils        = require('../utils');
var chalk         = require('chalk');
var exec          = require('child_process').exec;
var sass          = require('node-sass');
var combine       = require('combine-mq');
var cssmin        = require('cssmin');
var bourbon       = require('node-bourbon');
var prepend       = require('prepend-file');

console.log(chalk.yellow('[npm run build:css]'), config.isDev ? chalk.magenta(': DEV_MODE'): '');

// TODO: neesd a little more work around sourcemaps...

sass.render({
  file: config.css.input,
  outputStyle: 'expanded',
  includePaths: bourbon.includePaths,
  sourceMap: config.isDev,
  outFile: config.css.output,
}, function(error, result) {
  if (!error) {

    // Convert the buffer to UTF-8 string
    var buff = result.css.toString('utf8');

    // Combine Media Queries
    var combined = combine.parseCssString(buff, {
      src: buff
    });

    // Minify CSS
    var minified = cssmin(combined);

    // Write this result on the disk
    utils.writeFile(config.css.output, minified, function(err) {
      if (err) throw err;

        // Prepend Banner
        prepend(config.css.output, config.banner, function(err) {
          if (err) throw err;
        });
    });
  }
});
