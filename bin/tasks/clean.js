
"use strict";

// clean

var config        = require('../config');
var utils         = require('../utils');
var chalk         = require('chalk');
var fsp           = require('fs-path');

console.log(chalk.yellow('[npm run clean]'));

var input = utils.pathify([config.clean.input]);

fsp.remove(input, function(err) {
	if (err) throw err;
});
