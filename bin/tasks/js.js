
"use strict";

// build:js

var config        = require('../config');
var chalk         = require('chalk');
var browserify    = require('browserify');
var uglify        = require('uglifyjs');
var prepend       = require('prepend-file');
var fs            = require('fs-path');

// TODO: need to see if the watcher is too heavy here, this rebundles scripts that may not even require it...

console.log(chalk.yellow('[npm run build:js]'), config.isDev ? chalk.magenta(': DEV_MODE'): '');

/*!
 * Add File Banner
 *
 * @param  {String} src
 * @return {Void}
 */
var banner = function(src) {
  prepend(src, config.banner, function(err) {
    if (err) throw err;
  });
}

// Bundle each
config.js.scripts.forEach(function(s) {

  var bundler = new browserify();
  var path    = config.js.path + s;
  var input   = config.js.input + path;
  var output  = config.js.output + path;
  var min     = output.replace(/(\.js)/g, '.min.js');
  var map     = s.replace(/(\.js)/g, '.js.map');

  // var options = config.isDev ? { uglify: true, map: path + '.map', output: output + '.map' }: { uglify: true };
  var options = {
    //fromString: true,
    outSourceMap: map
  };

  // Start the bundler
  bundler
    .add(input)
    .bundle(function (error, src) {
      if (error) throw error;

      // Write file
      fs.writeFile(output, src, function(err) {
        if (err) throw err;

        // // Minify with Uglify
        // var result = uglify.minify(src.toString('utf8'), options);
        var result = uglify.minify(output, options);

        // Write the Map
        fs.writeFile(config.js.output + config.js.path + map, result.map, function(err) {
          if (err) throw err;
        });

        // Write the minified file
        fs.writeFile(min, result.code, function(err) {
          if (err) throw err;

          // Write the Banner
          banner(min);
        });

        // Write the Banner
        banner(output);
      });
    });
});
