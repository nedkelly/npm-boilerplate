
"use strict";

// watch

var config        = require('../config');
var gaze          = require('gaze');
var exec          = require('child_process').exec;
var chalk         = require('chalk');

console.log(chalk.yellow('[npm run watch]'));

config.watch.tasks.forEach( function(t) {
	gaze([t.path], function(err, watcher) {
		watcher.on('all', function(event, filepath) {
			console.log(chalk.yellow(event + ': ') + chalk.cyan(filepath));
			exec(t.task, function(err, stdout, stderr) {
				if (err) {
					console.log(chalk.red(`Watch Exec Error: ${err}`));
				}
			});
		});
	});
});
