
"use strict";

// copy

var config        = require('../config');
var utils         = require('../utils');
var chalk         = require('chalk');
var fsp           = require('fs-path');

console.log(chalk.yellow('[npm run copy]'));

config.copy.folders.forEach(function(folder) {

	var input = utils.pathify([config.copy.input, folder]);
	var output = utils.pathify([config.copy.output, folder]);

	fsp.copy(input, output, function(err, files) {
  	if (err) throw err;
	});
});
