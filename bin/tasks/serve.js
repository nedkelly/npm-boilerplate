
"use strict";

// serve

var config        = require('../config');
var chalk         = require('chalk');
var browserSync   = require('browser-sync').create();
var projectServer = require('project-server');

console.log(chalk.yellow('[npm run serve]'));

browserSync.init({
  files: config.browsersync.reload,
  server: {
      baseDir: config.browsersync.input,
      directory: false,
      middleware: [projectServer.projectServerIndex(config.browsersync.input, { config: config.browsersync.projectServer })],
  },
  notify: false,
  online: false,
  open: "local"
});
