
"use strict";

// build:docs

var config        = require('../config');
var utils         = require('../utils');
var chalk         = require('chalk');
var marked        = require('marked');
var fs            = require('fs');

console.log(chalk.yellow('[npm run build:docs]'));

var ext = /(.md)/g;

// Read the `docs` template
fs.readFile(config.docs.template, 'utf8', function (err, template) {

  // Loop through each `input` entry
  config.docs.input.forEach( function(d) {

    // Create a `filePath` relative to this `script`
    var filePath = utils.pathify([__dirname, '../../', d.dir]);

    // Get `files` in the supplied `input` directories
    utils.getFilesRecursive(filePath, d.recursive, ext).map( function(file) {

      // Read the `contents` of the `file`
      fs.readFile(file.path, 'utf8', function (err, markdown) {
        if (err) throw err;

        // Set the `outputPath`
        var outputPath = utils.pathify([config.docs.output, file.name.replace(ext, '.html')]);

        // Do the `markdown`
        marked(markdown, function (err, markup) {
          if (err) throw err;

          // Inject the resulting `markup` into the template
          var output = template
            .replace(/(<%= contents %>)/g, markup)
            .replace(/(<%= favicon %>)/g, config.docs.favicon);

          // Write this result to the file system
          utils.writeFile(outputPath, output, function(err) {
            if (err) throw err;
          });

        });
      });
    });
  });
});
