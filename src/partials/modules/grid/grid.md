# Basic Grid System

The included grid system is a simplified version of the Twitter Bootstrap semantic grid.

For a working prototype including optional usages see it on <a href="http://codepen.io/nedkelly/pen/WrEyKo/" target="_blank">CodePen</a>.

<p data-height="268" data-theme-id="22944" data-slug-hash="WrEyKo" data-default-tab="css" data-user="nedkelly" class="codepen">See the Pen <a href="http://codepen.io/nedkelly/pen/WrEyKo/">Basic Grid System</a> by Nathan Kelly (<a href="http://codepen.io/nedkelly">@nedkelly</a>) on <a href="http://codepen.io">CodePen</a>.</p>
<script async src="//assets.codepen.io/assets/embed/ei.js"></script>

## Elements

The grid uses 3 main elements, `.container`, `.row` & `.column`:

``` html
<div class="container">
  <div class="row">
    <div class="column">Column</div>
    <div class="column">Column</div>
  </div>
</div>
```

### Usage

Specify your own markup:

``` html
<div class="my-module">
  <div class="row">
    <div class="column content__main"><div class="content">Basic Grid</div></div>
    <div class="column content__aside"><div class="content">Yes, that's what I said...</div></div>
  </div>
</div>
```
Then add the SCSS:

``` scss
.my-module {
  .content--aside {
    @include columns(12);

    @include breakpoint(md) {
      @include columns(4);
    }
  }

  .content--main {
    @include columns(12);

    @include breakpoint(md) {
      @include columns(8);
    }
  }
}
```


This allows us to use regular breakpoint mixins and combine them where needed to create semantic, tidy CSS grids, no classitis mess in the HTML.
