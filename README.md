# NPM Boilerplate

Styleguide and codebase for all client-side code.

### What's in this build?

- [NPM](https://www.npmjs.com/)
- [Swig](http://paularmstrong.github.io/swig/)
- [LibSass](http://libsass.org/) with [Bourbon mixin library](http://bourbon.io/)
- [Browsersync](http://www.browsersync.io/) with [Project Server](https://www.npmjs.com/package/project-server)

### Getting started

To get started, simply follow these simple steps:

###### 1. Clone the repository

``` bash
git clone git@bitbucket.org:nedkelly/npm-boilerplate.git
cd npm-boilerplate
```

###### 2. Install dependencies

``` bash
npm install npm@latest -g
npm install
```

###### 3. Run the default npm script

``` bash
npm start
```

### Available NPM tasks

Run `npm run` followed by one of the tasks below to perform a specific action.

- **clean** - removes the `./public` directory
- **copy** - runs [copy], copies from `./src/assets/` to `./public/assets/`
- **build** - runs [build:css, build:js, build:swig, build:docs, copy]
- **build:css** - runs [build:css]
- **build:js** - runs [build:js]
- **build:swig** - runs [build:swig]
- **build:docs** - runs [build:docs]
- **rebuild** - runs [clean, build]
- **dev** - sets DEV_MODE and runs [build, watch, serve]
- **dev:rebuild** - sets DEV_MODE and runs [rebuild, watch, serve]
- **serve** - runs [watch, serve:bs]
- **serve:bs** - runs [serve:bs]
- **watch** - runs [watch]
- **start** - runs [dev]"
